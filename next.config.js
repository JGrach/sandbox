// eslint-disable-next-line
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

module.exports = () => {
    return {
        distDir: `build`,
        env: {
            APP_NAME: 'Vaccine Observation',
        },
        webpack: config => {
            if (!config.plugins) config.plugins = [];
            if (!config.resolve) config.resolve = {};
            if (!config.resolve.plugins) config.resolve.plugins = [];
            if (!config.module) config.module = { rules: [] };
            if (!config.module.rules) config.module.rules = [];

            config.resolve.plugins.push(
                new TsconfigPathsPlugin({
                    baseUrl: `${__dirname}`,
                    configFile: `${__dirname}/tsconfig.json`,
                    extensions: ['.ts', '.tsx', '.js'],
                }),
            );
            config.module.rules.push({
                test: /\.svg$/,
                use: ['@svgr/webpack'],
            });

            return config;
        },
    };
};
