import React from 'react';
import App, { AppProps } from 'next/app';
// import { Layout } from 'app/components/ui/layout';

interface MyAppProps extends AppProps {
    texts: any;
}

class MyApp extends App<MyAppProps> {
    public render() {
        const { Component, pageProps } = this.props;
        return (
            <>
                {/* <Layout componentName={Component.name || 'default'}> */}
                <Component {...pageProps} />
                {/* </Layout> */}
            </>
        );
    }
}

export default MyApp;
