import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
    color: red;
`;

const IndexPage = () => <Title>{process.env.APP_NAME}</Title>;

export default IndexPage;
