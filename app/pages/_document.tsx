import React, { ReactElement } from 'react';
import Document, { Html, Head, Main, NextScript, DocumentContext, DocumentProps } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

interface MyDocumentProps extends DocumentProps {
    styleTags: ReactElement[];
}

export default class MyDocument extends Document<MyDocumentProps> {
    public static async getInitialProps({ renderPage }: DocumentContext) {
        const sheet = new ServerStyleSheet();
        const page = renderPage(App => props => sheet.collectStyles(<App {...props} />));
        const styleTags = sheet.getStyleElement();
        return { ...page, styleTags };
    }

    public render() {
        return (
            <Html>
                <Head>{this.props.styleTags}</Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
