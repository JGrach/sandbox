import { links } from './linkInterface';

export default [
    {
        nameKey: 'homeLink',
        href: '/',
        as: '/',
    },
    {
        nameKey: 'aboutLink',
        href: '/about',
        as: '/about',
    },
] as links;
