export interface LinkInterface {
    nameKey: string;
    href: string;
    as: string;
}

export type links = LinkInterface[];
