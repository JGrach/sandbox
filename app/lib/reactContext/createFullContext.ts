import { createContext, useContext } from 'react';

const createFullContext = (defaultValue: any = null) => {
    const context = createContext(defaultValue);

    return {
        context,
        useContext: () => useContext(context),
        Provider: context.Provider,
        Consumer: context.Consumer,
    };
};

export default createFullContext;
