import React, { PropsWithChildren } from 'react';
import Head from 'next/head';
import { APP_NAME } from 'app/constants/common';
import { GlobalStyle } from 'app/components/ui/globalStyle';

interface LayoutProps {
    componentName: string;
}

const Layout = ({ children, componentName }: PropsWithChildren<LayoutProps>) => (
    <>
        <Head>
            <title>{`${APP_NAME}-${componentName || 'title'}`}</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>

        <GlobalStyle />
        {children}
    </>
);
export default Layout;
