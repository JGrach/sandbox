import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  div#__next, html, body {
    margin: 0;
    height: 100%;
    width: 100%;
  }
`;

export default GlobalStyle;
