import express, { Request, Response } from 'express';

const app = (port: number, handle: (req: Request, res: Response) => any) => () => {
    const server = express();

    server.use(express.json());

    server.get('*', (req, res) => {
        return handle(req, res);
    });

    server.listen(port, (err?: Error) => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
    });
};

export default app;
